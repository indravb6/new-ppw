"""new_ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from lab6 import views
from lab9.views import lab9index, get_book, toggle_favorite, logout_view
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('profile', views.profile, name='profile'),
    path('lab9', lab9index, name='lab9index'),
    path('get-book/', get_book, name='get_book'),
    path('toggle-favorite/', toggle_favorite, name='toggle_favorite'),
    path('login/', include('social_django.urls', namespace='social')),
    path('logout/', logout_view, name='logout'),
]
