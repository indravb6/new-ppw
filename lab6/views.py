from django.shortcuts import render
from .models import Post

def index(request):
    if request.method == "POST":
        data = dict(request.POST.items())
        if 'csrfmiddlewaretoken' in data.keys():
            del data['csrfmiddlewaretoken']
        post = Post(**data)
        try:
            post.full_clean()
            post.save()
        except Exception as e:
            pass
    return render(request, 'lab6_index.html', {"data" : list(Post.objects.all().order_by('-date'))})

def profile(request):
    return render(request, 'lab6_profile.html', {})
