from django.db import models
from django.utils import timezone

class Post(models.Model):
  status = models.CharField(max_length=300)
  date = models.DateTimeField(default=timezone.now, blank=True)
