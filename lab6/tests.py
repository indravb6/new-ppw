from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from .views import index, profile
from urllib.parse import urlencode
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep

from lab6.models import Post


class LandingPageFunctionalTestCase(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        super(LandingPageFunctionalTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(LandingPageFunctionalTestCase, self).tearDown()

    def test_submit_data(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        status = selenium.find_element_by_id('status')
        submit = selenium.find_element_by_id('submit')

        status.send_keys('Halooo')

        submit.send_keys(Keys.RETURN)

        sleep(2)

        self.assertIn('<h4>Halooo</h4>', selenium.page_source)

    def test_style(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        element = selenium.find_element_by_tag_name('h3')
        attribute_value = element.value_of_css_property('text-align')
        self.assertEqual(attribute_value, 'center')

        element = selenium.find_element_by_class_name('card')
        attribute_value = element.value_of_css_property('border-radius')
        self.assertEqual(attribute_value, '10px')

    def test_html(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        element = selenium.find_element_by_tag_name('form')
        attribute_value = element.get_attribute('method')
        self.assertEqual(attribute_value, 'post')

        element = selenium.find_element_by_tag_name('textarea')
        attribute_value = element.get_attribute('placeholder')
        self.assertEqual(attribute_value, 'Tuliskan statusmu ...')


class LandingPageTestCase(TestCase):
    def setUp(self):
        pass

    def test_header_text(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    def test_get_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_create_data(self):
        post = Post.objects.create(status="helooooo")
        post.save()
        self.assertEqual(Post.objects.count(), 1)

    def test_submit_valid_data(self):
        status = "this is my status"
        data = urlencode({"status": status})
        response = self.client.post(
            "/", data, content_type="application/x-www-form-urlencoded")
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn(status, html_response)

    def test_submit_invalid_data(self):
        status = "this is my status" * 300
        data = urlencode({"status": status})
        response = self.client.post(
            "/", data, content_type="application/x-www-form-urlencoded")
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertNotIn(status, html_response)


class ProfilePageFunctionalTestCase(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        super(ProfilePageFunctionalTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(ProfilePageFunctionalTestCase, self).tearDown()

    def test_html(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/profile")

        self.assertIn('Aktivitas dan Pengalaman', selenium.page_source)
        self.assertIn('Organisasi dan Kepanitiaan', selenium.page_source)
        self.assertIn('Prestasi', selenium.page_source)

    def test_loading(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/profile")

        element = selenium.find_element_by_id('content')
        attribute_value = element.value_of_css_property('display')
        self.assertEqual(attribute_value, 'none')

        sleep(4)

        element = selenium.find_element_by_id('content')
        attribute_value = element.value_of_css_property('display')
        self.assertEqual(attribute_value, 'block')


class ProfilePageTestCase(TestCase):

    def setUp(self):
        request = HttpRequest()
        response = profile(request)
        self.html_response = response.content.decode('utf8')

    def test_get_profile_page(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)

    def test_name(self):
        self.assertIn('Muhammad Indra Ramadhan', self.html_response)

    def test_email(self):
        self.assertIn('mindrar96@gmail.com', self.html_response)

    def test_phone_number(self):
        self.assertIn('+62 8777 426 1350', self.html_response)

    def test_address(self):
        self.assertIn('Ciputat, Tangerang Selatan.', self.html_response)
