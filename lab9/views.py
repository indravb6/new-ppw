from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Favorite
import requests


def get_book(request):
    try:
        keyword = request.GET["q"]
        data = requests.get(
            "https://www.googleapis.com/books/v1/volumes?q=" + keyword).json()["items"]
        new_data = {}
        for o in data:
            new_data[o["id"]] = {"title": o["volumeInfo"]["title"],
                                 "publisher": o["volumeInfo"]["publisher"],
                                 "link": o["volumeInfo"]["infoLink"],
                                 "image": o["volumeInfo"]["imageLinks"]["smallThumbnail"],
                                 "is_favorite": False}
        if not request.user.is_authenticated:
            favorites = Favorite.objects.all()
            for favorite in favorites:
                if favorite.book_id in new_data.keys():
                    new_data[favorite.book_id]["is_favorite"] = True
        else:
            for key in new_data.keys():
                if request.session.get(key, False):
                    new_data[key]["is_favorite"] = True

        return JsonResponse({'data': new_data})
    except:
        return JsonResponse({'data': {}})


def lab9index(request):
    if not request.user.is_authenticated:
        query = Favorite.objects.all()
        return render(request, "lab9_index.html", {'favorite_count': len(query)})
    else:
        request.session["email"] = request.user.email
        name = request.user.first_name + " " + request.user.last_name
        favorite_count = request.session.get('favorite_count', 0)
        return render(request, "lab9_index.html", {'favorite_count': favorite_count, "name": name, "avatar": request.user.password})


@csrf_exempt
def toggle_favorite(request):
    book_id = request.POST["book_id"]
    if not request.user.is_authenticated:
        query = Favorite.objects.filter(book_id=book_id)
        is_favorite = False
        if query:
            query[0].delete()
        else:
            favorite = Favorite(book_id=book_id)
            favorite.save()
            is_favorite = True
        favorite_count = len(Favorite.objects.all())
        return JsonResponse({'is_favorite': is_favorite, 'favorite_count': favorite_count})
    else:
        cnt = request.session.get('favorite_count', 0)
        is_favorite = not request.session.get(book_id, False)
        request.session[book_id] = is_favorite
        cnt += 1 if is_favorite else -1
        request.session['favorite_count'] = cnt
        return JsonResponse({'is_favorite': is_favorite, 'favorite_count': cnt})


def logout_view(request):
    logout(request)
    return redirect('/lab9')
