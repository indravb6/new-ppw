from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from urllib.parse import urlencode
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep


class Lab9FunctionalTestCase(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        super(Lab9FunctionalTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab9FunctionalTestCase, self).tearDown()

    def test_add_and_remove_favorite(self):
        selenium = self.selenium
        selenium.get("http://localhost:8000/lab9")

        keyword = selenium.find_element_by_id('keyword')
        submit = selenium.find_element_by_id('submit')

        keyword.send_keys('indonesia')
        submit.send_keys(Keys.RETURN)
        sleep(2)

        toggle_favorite = selenium.find_element_by_id('favorite_0')
        favorite_count = selenium.find_element_by_id('favorite_count')

        self.assertEqual(int(favorite_count.get_attribute('innerHTML')), 0)

        toggle_favorite.click()
        sleep(2)
        self.assertEqual(int(favorite_count.get_attribute('innerHTML')), 1)

        toggle_favorite.click()
        sleep(2)
        self.assertEqual(int(favorite_count.get_attribute('innerHTML')), 0)

    def test_html(self):
        selenium = self.selenium
        selenium.get("http://localhost:8000/lab9")

        sleep(2)
        self.assertIn('<table', selenium.page_source)
        self.assertIn('Login', selenium.page_source)
        sleep(2)
