$(document).ready(function() {
  const panel1 = $('#panel1');
  const panel2 = $('#panel2');
  const panel3 = $('#panel3');

  $('#flip1').click(function() {
    panel3.slideUp('slow');
    panel2.slideUp('slow');
    panel1.slideToggle('slow');
  });
  $('#flip2').click(function() {
    panel3.slideUp('slow');
    panel1.slideUp('slow');
    panel2.slideToggle('slow');
  });
  $('#flip3').click(function() {
    panel3.slideToggle('slow');
    panel1.slideUp('slow');
    panel2.slideUp('slow');
  });

  panel1.hide();
  panel2.hide();
  panel3.hide();
});
