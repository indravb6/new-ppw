const header = `
  <tr>
  <td style="width:15%;">
      <b>thumbnail</b>
    </td>
    <td style="width:70%;">
      <b>Title</b>
    </td>
    <td style="width:25%;">
      <b>Publisher</b>
    </td>
    <td>
      <b>Favorite</b>
    </td>
  </tr>`;

const book_id_map = {};

const fetch_book = () => {
  const keyword = $('#keyword').val();
  $('#book_list').hide();
  $('#loading').show();
  $.ajax({
    type: 'json',
    method: 'GET',
    url: '/get-book/?q=' + keyword,
    success: response => {
      let ret = header;
      let id = 0;
      Object.keys(response.data).forEach(book_id => {
        const row = response.data[book_id];
        ret += `
          <tr>
            <td><img src="${row.image}"/></td>
            <td><a target="_blank" href="${row.link}">${row.title}</a></td>
            <td>${row.publisher}</td>
            <td><center>
              <img 
                style="filter: grayscale(${row.is_favorite ? '0%' : '100%'}); cursor: pointer;" 
                src="${static_path}images/star-icon.png"
                id="favorite_${id}"
                onclick="toggle(${id})"
              />
            </center></td>
          </tr>`;
        book_id_map[id] = book_id;
        id++;
      });
      $('#loading').hide();
      if (Object.keys(response.data).length > 0) $('#book_list').show();
      else $('#book_list').hide();
      $('#book_list').html(ret);
    }
  });
};

const toggle = id => {
  $.ajax({
    type: 'json',
    method: 'POST',
    url: '/toggle-favorite/',
    data: { book_id: book_id_map[id] },
    success: response => {
      const { is_favorite, favorite_count } = response;
      if (is_favorite) {
        $('#favorite_' + id).css('filter', 'grayscale(0%)');
      } else {
        $('#favorite_' + id).css('filter', 'grayscale(100%)');
      }
      $('#favorite_count').html(favorite_count);
    }
  });
};

const search_key = e => {
  if (e.keyCode == 13) {
    fetch_book();
    return false;
  }
};
